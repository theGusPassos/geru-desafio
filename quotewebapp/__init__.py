# __init__.py

# Inicia o site e suas rotas, incluindo as rotas para consulta no banco

from wsgiref.simple_server import make_server
from pyramid.config import Configurator

def main():
    print('Initializing at localhost:5555')

    with Configurator() as config:
        '''
            Rotas do website
        '''
        config.add_route('home', '/')
        config.add_route('quotes', '/quotes')
        config.add_route('random_quote', '/quotes/random')
        config.add_route('single_quote', '/quotes/{id}')
        config.scan('quoteviews')

        '''
            Endpoints para consultas no banco
        '''
        config.add_route('query_all', '/api/sessions/queryall')
        config.add_route('query_session', '/api/sessions/query')
        config.add_route('insert_session', '/api/sessions/insert')
        config.scan('sessionapiviews')

        config.include('pyramid_chameleon')
        app = config.make_wsgi_app()
        
    server = make_server('0.0.0.0', 5555, app)
    server.serve_forever()

if __name__ == '__main__':
    main()