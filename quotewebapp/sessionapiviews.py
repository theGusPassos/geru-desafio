# sessionapiviews.py

from pyramid.view import view_config
import sessionapimodel

@view_config(route_name='insert_session', renderer='json')
def insert_session(request):

    """
        Manually inserts an Session() in the database

        session_id: the session id
        page: the page that the user has viewed
        datetime: when the user opened the base
    """

    return sessionapimodel.insert_session(request)

@view_config(route_name='query_session', renderer='json')
def query_session(request):

    """
        Query a session using the following parameters that
        must be passed in the query string

        (session_id, page, date)

        'the parameters are going to be used with an OR expression, not AND'
    """

    return sessionapimodel.query_session(request)

@view_config(route_name='query_all', renderer='json')
def query_all(request):
    
    """
        Query every row in the database
    """

    return sessionapimodel.query_all(request)