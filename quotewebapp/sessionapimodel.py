# sessionapimodel.py

from databaseconnector import SessionRegistry
import databaseconnector
import datetime

def insert_session(request):
    
    """
        Inserts an Session() in the database if the 
        request has the appropiate values
    """

    if 'session_id' in request.params and 'page' in request.params:
        databaseconnector.insert_sessions(SessionRegistry(          \
                            id = request.params['session_id'],      \
                            page = request.params['page'],          \
                            datetime = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") ))

        return { 'Message': 'Successs, registry with id {} saved in the database' \
                            .format(request.params['session_id']) }
    else:
        return { 'Exception': 'The query string params ' + \
        'session_id\' and \'page\' are mandatory for this request' }

def query_session(request):
    session_id = ''
    page = ''
    date = None

    if 'session_id' in request.params:
        session_id = request.params['session_id']
    if 'page' in request.params:
        page = request.params['page']
    if 'date' in request.params:
        date = datetime.datetime.strptime(request.params['date'], "%Y-%m-%d %H:%M:%S")

    if session_id is '' and page is '' and date is None:
        return { 'Exception': 'To query a session, you need to provide one of those params: ' + \
                                '(session_id, page, date) in the query string' }

    query_response = \
        databaseconnector.query_session(SessionRegistry(id = session_id, page = page, datetime = date))
    response_in_json = sessionrow_to_json(query_response)

    return { 'Message': response_in_json }

def query_all(request):

    """
        Returns every row in the database in a json
    """

    query_response = databaseconnector.query_all_sessions()
    response_in_json = sessionrow_to_json(query_response)
    return { 'Message': response_in_json }

def sessionrow_to_json(session_row):

    """
        Returns a json with the Session items
    """

    json_list = []
    if isinstance(session_row, list):
        for row in session_row:
            json_list.append( \
                {   'session_id': row.id, 
                    'page': row.page,
                    'date': str(row.datetime) } )
    else:
        return { 'session_id': row.id, 'page': row.page, 'date': str(row.datetime) }

    return json_list