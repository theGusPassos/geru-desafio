# quoteviews.py

# Views do desafio web

from pyramid.view import view_config
import quotemodel

@view_config(route_name='home')
def home_view(request):

    """
        website's home page

        Path = {url}\\
    """

    return quotemodel.get_homepage(request)

@view_config(route_name='quotes')
def quotes_view(request):

    """
        page with every quote registred in the quote api

        Path = {url}\\quotes
    """

    return quotemodel.get_every_quote(request)

# Mostra apenas uma frase
@view_config(route_name='single_quote')
def single_quote_view(request):

    """
        page with a single quote registred in the quote api

        what defines which quote will be shown, is the `id` in the
        request url

        Path = {url}\\quotes\\{id}
    """

    return quotemodel.get_single_quote(request)

# Mostra uma frase random
@view_config(route_name='random_quote')
def random_quote_view(request):

    """
        page with a single random quote

        Path = {url}\\quotes\\random
    """

    return quotemodel.get_random_quote(request)




