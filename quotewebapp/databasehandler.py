# databasehandler.py

import databaseconnector
from databaseconnector import SessionRegistry
from datetime import datetime

def insert_sessiondata(cookie, page):
    new_session = SessionRegistry(id = cookie, page = page, datetime = datetime.now())
    databaseconnector.insert_sessions(new_session)