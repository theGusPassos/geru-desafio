# databaseconnector.py

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, String, Date, Integer
from sqlalchemy.orm import sessionmaker
from sqlalchemy import or_
import datetime

Base = declarative_base()
Engine = create_engine('sqlite:///:memory:', echo=True)
Session = sessionmaker(bind=Engine)

class SessionRegistry(Base):
    __tablename__ = 'Sessions'

    uniq_id = Column(Integer, primary_key = True)
    id = Column(String)
    datetime = Column(Date)
    page = Column(String)

Base.metadata.create_all(Engine)

def insert_sessions(session_registry):

    """
        Inserts a row in the database

        session_registry: an Session() object
    """

    session = Session()
    session.add(session_registry)
    session.commit()

def query_session(session_registry):

    """
        Query sessions in the database using the OR
        operator in the values from the session_registry
    """

    session = Session()
    query = session.query(SessionRegistry).filter(  or_( SessionRegistry.id == session_registry.id,
                                                        SessionRegistry.page == session_registry.page,
                                                        SessionRegistry.datetime == session_registry.datetime) )   
    queried_items = []

    for registry in query:
        queried_items.append(registry)

    return queried_items

def query_all_sessions():

    """
        Query every row in the database
    """

    session = Session()
    query = session.query(SessionRegistry)
    queried_items = []

    for registry in query:
        queried_items.append(registry)
    
    return queried_items