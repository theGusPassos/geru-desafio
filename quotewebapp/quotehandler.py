# quotehandler.py

# formata as frases carregadas
# da API

def get_quotes_formatted(quotes):

    """
        Returns a quote list formatted
        as html lines
    """

    formatted = ''
    for quote in quotes:
        formatted += get_quote_formatted(quote)
    return formatted

def get_quote_formatted(quote):

    """
        Returns a quote formatted as html
    """

    return '<p>{}</p>'.format(quote)