# quotemodel.py

from pyramid.response import Response
import cookiehandler
import databasehandler
import quoteapi
import quotehandler
import random

home_url = 'http://localhost:5555'
all_quotes_path = '/quotes'
single_quote_path = '/quotes/1'
random_quote_path = '/quotes/random'

def get_homepage(request):

    """
       Returns the site home page with links for the other pages 
    """

    response = Response("<h1>Desafio Web 1.0</h1>" + get_links())
    cookie = cookiehandler.set_response_cookie(request, response)
    databasehandler.insert_sessiondata(cookie, 'home page')
    
    return response

def get_every_quote(request):

    """
        Returns a page with every quote queried from the quote api
    """

    try:
        response = quoteapi.get_quotes()
        # testa se as frases estão na request
        if (response['quotes'] is not None and len(response['quotes']) > 0):
            # formata as frases presentes na request para o formato html
            quotes_formatted = quotehandler.get_quotes_formatted(response['quotes'])
            response = Response('<h1>Todas as frases</h1>' + quotes_formatted)
            cookie = cookiehandler.set_response_cookie(request, response)
            databasehandler.insert_sessiondata(cookie, 'every quote page')

            return response

        handled_error = handle_response_error(response)
        return handled_error

    except Exception as ex:
        # Mostra um link para home caso uma exceção não tratada ocorra
        print('Ocorreu um erro ao buscar todas as frases: ' + str(ex))
        return get_exception_response('Server', str(ex), home_url)

def get_single_quote(request):

    """
        Returns a page with the quote specified in the
        request params
    """

    # busca pelo id fornecido pelo usuário na request
    quote_id = request.matchdict['id']
    
    if (quote_id is not None):
        try:
            # chama a api das frases utilizando o id encontrado na request
            api_response = quoteapi.get_quote(quote_id)

            if (api_response['quote'] is not None):
                response = Response('<h1>Frase id: {}</h1>'.format(quote_id) + api_response['quote'])
                cookie = cookiehandler.set_response_cookie(request, response)
                databasehandler.insert_sessiondata(cookie, 'single quote page')

                return response
            
            handled_error = handle_response_error(api_response)
            return handled_error

        except Exception as ex:
            # Mostra um link para home caso uma exceção não tratada ocorra
            print('Ocorreu um erro ao buscar a frase de id: {}: {}'.format(quote_id, str(ex)))
            return get_exception_response('Server', str(ex), home_url)
    
    return get_exception_response('Server', 'É necessário fornecer um ID para a query da frase', home_url)

def get_random_quote(request):

    """
        Returns a page with a random quote
    """

    try:
        api_response = quoteapi.get_quotes()

        # após buscar todas as frases, sorteia uma para mostrar ao usuário
        if (api_response['quotes'] is not None and len(api_response['quotes']) > 0):
            selected_quote = random.choice(api_response['quotes'])
            response = Response('<h1>Frase random</h1>' + quotehandler.get_quote_formatted(selected_quote))
            cookie = cookiehandler.set_response_cookie(request, response)
            databasehandler.insert_sessiondata(cookie, 'random quote page')

            return response
        
        handled_error = handle_response_error(api_response)
        return handled_error

    except Exception as ex:
        # Mostra um link para home caso uma exceção não tratada ocorra
        print('Ocorreu um erro ao buscar uma frase randomica: ' + str(ex))
        return get_exception_response('Server', str(ex), home_url)

def handle_response_error(response):
    
    """
        Handle possible errors while using the api to
        retrieve quotes and returns an Response with 
        a formatted html line
    """

    if (response['message'] is not None):
        return get_exception_response('Quote API', response['message'], home_url)

    elif (response['error'] is not None):
        return get_exception_response('Quote API', response['error'], home_url)

    else:
        raise Exception('Erro não identificado')

def get_exception_response(service, error, redirect):

    """
        Returns an generic error html line with the error
        and a link to the website homepage
    """

    return Response('<h1>{} exception: {}<h1/><a href="{}">Página inicial</a>'.format(service, error, redirect))


def get_links():

    """
        Returns every link for this website in html tags
    """

    links = '<br/><a href="{}">Todas as frases</a>' +       \
            '<br/><a href="{}">Uma frase por página</a>' +  \
            '<br/><a href="{}">Frases randomicas</a>'

    return links.format(home_url + all_quotes_path, 
                        home_url + single_quote_path, 
                        home_url + random_quote_path)