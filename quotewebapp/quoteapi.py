# quoteapi.py

# Abstrai a conexão com a api de frases

import requests

url = 'https://1c22eh3aj8.execute-api.us-east-1.amazonaws.com/'
quote_path = 'challenge/quotes/'

def get_quotes():

    """
        Returns every quote in the quote api
    """

    response = requests.get(url+quote_path)
    data = response.json()
    return data

def get_quote(quote_number):

    """
        Returns a single quote in the quote api

        quote_number: the id that will be used in the api query 
        to retrieve the quote
    """

    response = requests.get(url+quote_path+str(quote_number))
    data = response.json()
    return data