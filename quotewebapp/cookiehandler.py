# cookiehandler.py

from pyramid.response import Response
import random
import string

cookie_key = 'ck_quote_website'
cookie_length = 50

def set_response_cookie(request, response):

    """
        Sets a cookie in the response if the request doesn't
        have one

        request: the received request
        response: the response that will be sent to the user
    """
    
    cookie = ''
    if cookie_key in request.cookies:
        cookie = request.cookies[cookie_key]
    else:
        cookie = generate_cookie()
        response.set_cookie(cookie_key, cookie, max_age=100000)

    return cookie

def generate_cookie():
    return ''.join(random.choices(string.ascii_uppercase + string.digits, k=cookie_length))
    